# Migrating pkg-go to salsa

``generate-map-already-migrated.py``: generates a rewrite map for already migrated packages.

``migrate-pkg-go-to-salsa.py``: migrates packages and generates a rewrite map

``get-pkg-go-emails.py``: parse alioth web pages for pkg-go members emails

``configure-all-projects.py``: configures all of the team's projects (setup webhooks and protected branches).

#!/usr/bin/python3

import subprocess
import sys
import os
import time
from distutils.util import strtobool

import requests


def get_alioth_repositories_list():
    repositories = []

    output = subprocess.check_output(
        [
            "ssh",
            "git.debian.org",
            "ls -l --format=commas /git/pkg-go/packages-to-migrate",
        ]
    ).decode("utf-8")

    for repository in output.split(","):
        repositories.append(repository.strip())

    return repositories


def archive_on_alioth(repository):
    remove_process = subprocess.Popen(
        [
            "ssh",
            "git.debian.org",
            "mv /git/pkg-go/packages-to-migrate/%s /git/pkg-go/packages-migrated-to-salsa/%s" % (repository, repository),
        ],
    )

    if remove_process.wait() != 0:
        raise Exception("Could not remove %s from alioth" % (repository))


def append_to_map(repository):
    project = repository.replace(".git", "")

    with open("alioth_rewrite_map.txt", "a") as myfile:
        myfile.write(
            "pkg-go/packages/{project} go-team/packages/{project}\n".format(
                project=project,
            )
        )


def clone_repository(repository, destination):
    print("Cloning", repository, "to", destination)

    git_clone_process = subprocess.Popen(
        [
            "git",
            "clone",
            "--depth=1",
            "git.debian.org:/git/pkg-go/packages-to-migrate/%s" % (repository),
            destination,
        ],
    )

    if git_clone_process.wait() != 0:
        raise Exception("Could not clone %s" % (repository))

    print("Cloned", repository)


def post_to_salsa_api(path, payload):
    r = requests.post(
        "https://salsa.debian.org/api/v4" + path,
        headers={"PRIVATE_TOKEN": os.environ["SALSA_TOKEN"]},
        data=payload,
    )
    r.raise_for_status()
    return r.json()


def create_webhook_on_salsa_or_log(repository, project_id, url):
    try:
        create_webhook_on_salsa(project_id, url)
    except Exception as ex:
        print("cannot create webhook for", repository)
        log_failure(repository, str(ex))


def protect_branch_on_salsa_or_log(repository, project_id, branch):
    try:
        protect_branch_on_salsa(project_id, branch)
    except Exception as ex:
        print("cannot protect branch", branch, "for", repository)
        log_failure(repository, str(ex))


def protect_branch_on_salsa(project_id, branch):
    path = "/projects/{project_id}/protected_branches".format(
        project_id=project_id,
    )

    payload = {
        "name": branch,
    }

    post_to_salsa_api(path, payload)


def create_webhook_on_salsa(project_id, url):
    path = "/projects/{project_id}/hooks".format(
        project_id=project_id,
    )

    payload = {
        "url": url,
        "push_events": True,
    }

    post_to_salsa_api(path, payload)


def create_repository_on_salsa(repository):
    project_name = repository.replace(".git", "")

    print("Creating repository for", project_name, "on salsa...")

    payload = {
        "path": project_name,
        "namespace_id": 2638,
        "description": "Debian packaging for %s" % (project_name),
        "visibility": "public",
        "import_url": "https://anonscm.debian.org/git/pkg-go/packages-to-migrate/%s" % (repository),
    }

    salsa_project = post_to_salsa_api("/projects", payload)

    return salsa_project


def user_yes_no_query():
    while True:
        try:
            return strtobool(input().lower()) == 1
        except ValueError:
            sys.stdout.write('Please respond with "y" or "n".\n')


def debtag(directory):
    debtag_process = subprocess.Popen(
        [
            "debtag",
        ],
        cwd=directory,
    )

    if debtag_process.wait() != 0:
        raise Exception("Could not debtag directory %s" % (directory))


def run_git_command(git_repository_path, *args):
    command = [
        "git",
        "--work-tree=" + git_repository_path,
        "--git-dir=" + os.path.join(git_repository_path, ".git"),
    ]
    command = command + list(args)
    git_process = subprocess.Popen(command)
    return_code = git_process.wait()
    return return_code


def git_push_all(git_repository_path):
    return_code = run_git_command(
        git_repository_path,
        "push",
        "origin",
        "--all",
    )

    if return_code != 0:
        raise Exception("Could push all for repository %s" % (git_repository_path))


def replace_vcs_urls(cloned_repository_path, vcs_browser, vcs_git):
    debian_control_path = os.path.join(
        cloned_repository_path,
        "debian",
        "control",
    )

    sed_script = "s/vcs-browser.*/Vcs-Browser: {vcs_browser}/gI;".format(
        vcs_browser=vcs_browser.replace("/", r"\/"),
    )
    sed_script += "s/vcs-git.*/Vcs-Git: {vcs_git}/gI;".format(
        vcs_git=vcs_git.replace("/", r"\/"),
    )

    sed_process = subprocess.Popen(
        [
            "sed",
            "-i",
            sed_script,
            debian_control_path,
        ]
    )

    if sed_process.wait() != 0:
        raise Exception(
            "Could not replace vcs urls in {file}".format(
                file=debian_control_path
            )
        )

    print("Replaced vcs urls in", debian_control_path)


def set_origin_url(git_repository_path, url):
    return_code = run_git_command(
        git_repository_path,
        "remote",
        "set-url",
        "origin",
        url,
    )

    if return_code != 0:
        raise Exception("Could set url of %s" % (git_repository_path))

    print("Set origin url of", git_repository_path, "to", url)


def wait_for_gitlab_import(project_id):
    start_time = time.time()

    while (time.time() - start_time) < 120:
        r = requests.get(
            "https://salsa.debian.org/api/v4/projects/{project_id}".format(
                project_id=project_id
            ),
            headers={"PRIVATE_TOKEN": os.environ["SALSA_TOKEN"]},
        )

        project = r.json()

        import_status = project["import_status"]
        project_name = project["name"]

        if import_status == "finished":
            print("Project", project_name, "was created on salsa")
            return

        print("Project import_status is still", import_status, "...")
        time.sleep(1)

    raise Exception(
        "Timed out while waiting for gitlab to import {project_id}".format(
            project_id=project_id,
        )
    )


def git_add(git_repository_path, path):
    return_code = run_git_command(
        git_repository_path,
        "add",
        path,
    )

    if return_code != 0:
        raise Exception(
            "Could not add {path} in {repo}".format(
                path=path,
                repo=git_repository_path,
            )
        )

    print("Added", path, "to", git_repository_path)


def git_commit(git_repository_path, commit_message):
    return_code = run_git_command(
        git_repository_path,
        "commit",
        "-m",
        commit_message,
    )

    if return_code != 0:
        raise Exception(
            "Could not commit in {repo}".format(
                repo=git_repository_path,
            )
        )

    print("Commited in", git_repository_path)


def get_packages_directory():
    packages_directory = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "packages",
    )
    return packages_directory


def get_cloned_repository_path(repository):
    cloned_repository_path = os.path.join(
        get_packages_directory(),
        repository,
    )
    return cloned_repository_path


def migrate_repository(repository):
    # Clean packages directory
    packages_directory = get_packages_directory()
    remove_directory_content(packages_directory)

    # Clone the repository
    cloned_repository_path = get_cloned_repository_path(repository)
    clone_repository(repository, cloned_repository_path)

    # Create it on salsa
    salsa_repository = create_repository_on_salsa(repository)
    salsa_repository_http_url = salsa_repository["http_url_to_repo"]
    salsa_repository_web_url = salsa_repository["web_url"]
    salsa_repository_ssh_url = salsa_repository["ssh_url_to_repo"]
    salsa_project_id = salsa_repository["id"]
    salsa_project_name = salsa_repository["name"]

    # set origin url
    set_origin_url(cloned_repository_path, salsa_repository_ssh_url)

    # Create webhooks
    create_webhook_on_salsa_or_log(
        repository,
        salsa_project_id,
        "https://webhook.salsa.debian.org/tagpending/{project_name}".format(
            project_name=salsa_project_name,
        )
    )

    # Protect branches
    protect_branch_on_salsa_or_log(repository, salsa_project_id, "master")
    protect_branch_on_salsa_or_log(repository, salsa_project_id, "debian/*")
    protect_branch_on_salsa_or_log(repository, salsa_project_id, "upstream")
    protect_branch_on_salsa_or_log(repository, salsa_project_id, "upstream/*")
    protect_branch_on_salsa_or_log(repository, salsa_project_id, "pristine-tar")

    # Replace VCS-* urls, dch, and commit
    has_git_url = contains_git_url(
        os.path.join(cloned_repository_path, "debian", "control")
    )

    replace_vcs_urls(
        cloned_repository_path=cloned_repository_path,
        vcs_browser=salsa_repository_web_url,
        vcs_git=salsa_repository_http_url,
    )

    run_dch(
       os.path.join(cloned_repository_path, "debian", "changelog"),
       "Point Vcs-* urls to salsa.debian.org.",
    )

    if has_git_url:
        dch_release(
            os.path.join(cloned_repository_path, "debian", "changelog"),
        )

    git_add(cloned_repository_path, "debian/control")
    git_add(cloned_repository_path, "debian/changelog")
    git_commit(cloned_repository_path, "point Vcs-* urls to salsa.debian.org")


    # upload packages with git://
    if has_git_url:
        debtag(cloned_repository_path)
        get_source(salsa_project_name, packages_directory)
        build_package(cloned_repository_path)

        print("May I upload", salsa_project_name, "? [y/n]")
        if user_yes_no_query() is False:
            raise Exception("did not upload %s" % (salsa_project_name))

        dput(packages_directory + "/*changes")

    # Wait for import to complete
    wait_for_gitlab_import(salsa_project_id)

    # Push repository
    git_push_all(cloned_repository_path)
    git_push_tags(cloned_repository_path)

    # Append to map
    append_to_map(repository)

    # Archive on alioth
    archive_on_alioth(repository)

    print("See migrated repository at", salsa_repository_web_url)

def contains_git_url(debian_control_path):
    with open(debian_control_path, "r") as f:
        if "git://" in f.read():
            return True
    return False


def dput(path):
    dput_process = subprocess.Popen(
        [
            "bash",
            "-c",
            "dput " + path,
        ],
    )

    if dput_process.wait() != 0:
        raise Exception("Could not dput path %s" % (path))


def git_push_tags(git_repository_path):
    return_code = run_git_command(
        git_repository_path,
        "push",
        "origin",
        "--tags",
    )

    if return_code != 0:
        raise Exception("Could push tags for repository %s" % (directory))


def get_source(package, directory):
    apt_process = subprocess.Popen(
        [
            "apt-get",
            "source",
            "-t",
            "unstable",
            package,
        ],
        cwd=directory,
    )

    if apt_process.wait() != 0:
        raise Exception("Could not get source of package %s" % (package))


def build_package(directory):
    dpkg_process = subprocess.Popen(
        [
            "dpkg-buildpackage",
            "-S",
            "--no-check-builddeps",
        ],
        cwd=directory,
    )

    if dpkg_process.wait() != 0:
        raise Exception("Could not build package %s" % (directory))


def remove_directory_content(directory):
    rm_process = subprocess.Popen(
        [
            "rm -rf " + directory + "/*",
        ],
        shell=True,
    )

    if rm_process.wait() != 0:
        raise Exception("Could not remove content of %s" % (directory))


def run_dch(changelog_file_path, message):
    dch_process = subprocess.Popen(
        [
            "dch",
            "--changelog",
            changelog_file_path,
            "--distribution",
            "UNRELEASED",
            message,
        ],
    )

    if dch_process.wait() != 0:
        raise Exception("Could not run dch on %s" % (changelog_file_path))


def dch_release(changelog_file_path):
    dch_process = subprocess.Popen(
        [
            "dch",
            "--changelog",
            changelog_file_path,
            "--release",
            "",
        ],
    )

    if dch_process.wait() != 0:
        raise Exception("Could not run dch on %s" % (changelog_file_path))


def log_failure(repository, message):
    with open("failures.txt", "a") as f:
        f.write(
            "{repository}: {message}\n".format(
                repository=repository,
                message=message,
            )
        )


def migrate_repositories(repositories):
    for repository in repositories:
        try:
            # Ask for permission before migrating
            #print("Should we migrate", repository, "to salsa? [y/n]")
            #if user_yes_no_query() is False:
            #    print(
            #        "Not migrating",
            #        repository,
            #        "on salsa (requested by user)",
            #    )
            #    continue

            migrate_repository(repository)

        except Exception as ex:
            print("Could not migrate package", repository, ":", ex)

            print("Push and archive anyway?")
            if user_yes_no_query() is True:
                archive_on_alioth(repository)
                try:
                    cloned_repository_path = get_cloned_repository_path(
                        repository
                    )
                    git_push_all(cloned_repository_path)
                    git_push_tags(cloned_repository_path)
                except Exception as ex:
                    print("Couldn't push, thats okay? Say no and I'll log it")
                    if user_yes_no_query() is False:
                        log_failure(repository, str(ex))
            else:
                log_failure(repository, str(ex))


def main():
    # Check for SALSA_TOKEN
    if "SALSA_TOKEN" not in os.environ:
        print("Please set the SALSA_TOKEN environement variable")
        sys.exit(1)

    try:
        repositories = get_alioth_repositories_list()
        migrate_repositories(repositories)
    except KeyboardInterrupt:
            print("\nExiting...")


if __name__ == "__main__":
    main()

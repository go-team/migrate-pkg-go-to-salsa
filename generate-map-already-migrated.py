#!/usr/bin/python3

import requests
import os

r = requests.get("https://salsa.debian.org/api/v4/groups/2638")

group = r.json()

projects = group['projects']


with open("alioth_rewrite_map.txt", "a") as myfile:
    for project in projects:
        project_name = project["name"]
        myfile.write(
            "pkg-go/packages/%s go-team/packages/%s\n" % (project_name, project_name)
        )

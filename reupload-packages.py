#!/usr/bin/python3

import os
import sys
import requests
import subprocess
from distutils.util import strtobool


def get_projects():
    r = requests.get(
        "https://salsa.debian.org/api/v4/groups/2638",
    )
    projects = reversed(r.json()["projects"])
    return projects


def run_dch(changelog_file_path, message):
    dch_process = subprocess.Popen(
        [
            "dch",
            "--changelog",
            changelog_file_path,
            "--team",
            message,
        ],
    )

    if dch_process.wait() != 0:
        raise Exception("Could not run dch on %s" % (changelog_file_path))

    dch_process = subprocess.Popen(
        [
            "dch",
            "--release",
            "--changelog",
            changelog_file_path,
            "",
        ],
    )

    if dch_process.wait() != 0:
        raise Exception("Could not run dch release on %s" % (changelog_file_path))


def reupload_project(project):
    project_name = project["name"]

    print(
        "Reuploading {project_name}...".format(
            project_name=project_name,
        )
    )

    packages_directory = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "packages",
    )

    remove_directory_content(packages_directory)

    # Clone the repository
    cloned_repository_path = os.path.join(packages_directory, project_name)
    clone_repository(project["ssh_url_to_repo"], cloned_repository_path)

    # Replace vcs urls, add, and commit
    vcs_browser = "https://salsa.debian.org/go-team/packages/" + project_name
    vcs_git = vcs_browser + ".git"
    replace_vcs_urls(
        cloned_repository_path=cloned_repository_path,
        vcs_browser=vcs_browser,
        vcs_git=vcs_git,
    )
    run_dch(
       os.path.join(cloned_repository_path, "debian", "changelog"),
       "Vcs-* urls: pkg-go-team -> go-team.",
    )
    git_add(cloned_repository_path, "debian/control")
    git_add(cloned_repository_path, "debian/changelog")
    git_commit(cloned_repository_path, "Vcs-* urls: pkg-go-team -> go-team")
    debtag(cloned_repository_path)

    # Get the source
    get_source(project_name, packages_directory)

    # Build it
    build_package(cloned_repository_path)

    # Upload
    dput(packages_directory + "/*changes")

    # git push
    git_push_all(cloned_repository_path)
    git_push_tags(cloned_repository_path)

    add_to_reuploaded_projects(project_name)


def dput(path):
    dput_process = subprocess.Popen(
        [
            "bash",
            "-c",
            "dput " + path,
        ],
    )

    if dput_process.wait() != 0:
        raise Exception("Could not dput path %s" % (path))


def git_push_all(git_repository_path):
    return_code = run_git_command(
        git_repository_path,
        "push",
        "origin",
        "--all",
    )

    if return_code != 0:
        raise Exception("Could push all for repository %s" % (directory))


def git_push_tags(git_repository_path):
    return_code = run_git_command(
        git_repository_path,
        "push",
        "origin",
        "--tags",
    )

    if return_code != 0:
        raise Exception("Could push tags for repository %s" % (directory))


def build_package(directory):
    dpkg_process = subprocess.Popen(
        [
            "dpkg-buildpackage",
            "-S",
        ],
        cwd=directory,
    )

    if dpkg_process.wait() != 0:
        raise Exception("Could not build package %s" % (package))


def debtag(directory):
    debtag_process = subprocess.Popen(
        [
            "debtag",
        ],
        cwd=directory,
    )

    if debtag_process.wait() != 0:
        raise Exception("Could not debtag directory %s" % (directory))


def get_source(package, directory):
    apt_process = subprocess.Popen(
        [
            "apt-get",
            "source",
            "-t",
            "unstable",
            package,
        ],
        cwd=directory,
    )

    if apt_process.wait() != 0:
        raise Exception("Could not get source of package %s" % (package))


def run_git_command(git_repository_path, *args):
    command = [
        "git",
        "--work-tree=" + git_repository_path,
        "--git-dir=" + os.path.join(git_repository_path, ".git"),
    ]
    command = command + list(args)
    git_process = subprocess.Popen(command)
    return_code = git_process.wait()
    return return_code


def git_add(git_repository_path, path):
    return_code = run_git_command(
        git_repository_path,
        "add",
        path,
    )

    if return_code != 0:
        raise Exception(
            "Could not add {path} in {repo}".format(
                path=path,
                repo=git_repository_path,
            )
        )

    print("Added", path, "to", git_repository_path)


def git_commit(git_repository_path, commit_message):
    return_code = run_git_command(
        git_repository_path,
        "commit",
        "-m",
        commit_message,
    )

    if return_code != 0:
        raise Exception(
            "Could not commit in {repo}".format(
                repo=git_repository_path,
            )
        )

    print("Commited in", git_repository_path)


def git_push(git_repository_path):
    return_code = run_git_command(
        git_repository_path,
        "push",
        "origin",
        "--all",
    )

    if return_code != 0:
        raise Exception("Could not push %s" % (git_repository_path))

    print("Pushed", git_repository_path)


def remove_directory_content(directory):
    rm_process = subprocess.Popen(
        [
            "rm -rf " + directory + "/*",
        ],
        shell=True,
    )

    if rm_process.wait() != 0:
        raise Exception("Could not remove content of %s" % (directory))


def clone_repository(url, destination):
    print("Cloning", url, "to", destination)

    git_clone_process = subprocess.Popen(
        [
            "gbp",
            "clone",
            url,
            destination,
        ],
    )

    if git_clone_process.wait() != 0:
        raise Exception("Could not clone %s" % (url))

    print("Cloned", url)


def replace_vcs_urls(cloned_repository_path, vcs_browser, vcs_git):
    debian_control_path = os.path.join(
        cloned_repository_path,
        "debian",
        "control",
    )

    sed_script = "s/vcs-browser.*/Vcs-Browser: {vcs_browser}/gI;".format(
        vcs_browser=vcs_browser.replace("/", r"\/"),
    )
    sed_script += "s/vcs-git.*/Vcs-Git: {vcs_git}/gI;".format(
        vcs_git=vcs_git.replace("/", r"\/"),
    )

    sed_process = subprocess.Popen(
        [
            "sed",
            "-i",
            sed_script,
            debian_control_path,
        ]
    )

    if sed_process.wait() != 0:
        raise Exception(
            "Could not replace vcs urls in {file}".format(
                file=debian_control_path
            )
        )

    print("Replaced vcs urls in", debian_control_path)


def user_yes_no_query():
    while True:
        try:
            return strtobool(input().lower()) == 1
        except ValueError:
            sys.stdout.write('Please respond with "y" or "n".\n')


def reupload_projects(projects):
    already_reuploaded_projects = get_already_reuploaded_projects()

    for project in projects:

        project_name = project["name"]

        if project_name in already_reuploaded_projects:
            print("Skipping already uploaded", project_name)
            continue

        try:
            print(
                "Should we reupload {project_name}?".format(
                    project_name=project_name,
                )
            )
            if user_yes_no_query() is False:
                print("Not reuploading", project_name)
                continue

            reupload_project(project)

        except Exception as ex:
            print("Could not mmigrateigrate project", project_name, ":", ex)
            log_failed_project_migration(project_name, str(ex))


def log_failed_project_migration(project_name, message):
    with open("failures.txt", "a") as f:
        f.write(
            "{project_name}: {message}\n".format(
                project_name=project_name,
                message=message,
            )
        )


def get_already_reuploaded_projects():
    reuploaded_projects = []
    with open("reuploaded.txt") as f:
        for project in f:
            reuploaded_projects.append(
                project.strip(),
            )
    return reuploaded_projects


def add_to_reuploaded_projects(project_name):
    with open("reuploaded.txt", "a") as f:
        f.write(project_name.strip() + "\n")


def main():
    # Check for SALSA_TOKEN
    if "SALSA_TOKEN" not in os.environ:
        print("Please set the SALSA_TOKEN environement variable")
        sys.exit(1)

    try:
        projects = get_projects()
        reupload_projects(projects)
    except KeyboardInterrupt:
            print("\nExiting...")


if __name__ == "__main__":
    main()

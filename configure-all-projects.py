#!/usr/bin/python3

import os
import sys
import requests
import urllib.parse
from distutils.util import strtobool


def request_to_salsa_api(method, path, data=None):
    r = requests.request(
        method,
        "https://salsa.debian.org/api/v4" + path,
        headers={"PRIVATE_TOKEN": os.environ["SALSA_TOKEN"]},
        data=data,
    )
    r.raise_for_status()

    try:
        return r.json()
    except ValueError:
        return None


def get_projects():
    group = request_to_salsa_api(
        "GET",
        "/groups/2638",
    )
    projects = group["projects"]
    return projects


def configure_protected_branches(project):
    project_id = project["id"]

    protected_branches = request_to_salsa_api(
        "GET",
        "/projects/{project_id}/protected_branches".format(
            project_id=project_id,
        )
    )
    protected_branches_names = [
        branch["name"] for
        branch in protected_branches
    ]

    for branch in ["master", "debian/*", "upstream",
                   "upstream/*", "pristine-tar"]:
        if branch not in protected_branches_names:
            print("    protecting branch", branch, "...")
            protect_branch_on_salsa(project_id, branch)
        else:
            print("    updating protected branch", branch)
            protect_branch_on_salsa(project_id, branch, update=True)


def configure_webhooks(project):
    project_id = project["id"]
    project_name = project["name"]

    existing_webhooks = request_to_salsa_api(
        "GET",
        "/projects/{project_id}/hooks".format(
            project_id=project_id,
        )
    )
    existing_webhooks_urls = [webhook["url"] for webhook in existing_webhooks]

    webhooks_to_configure = []

    webhooks_to_configure.append(
        "https://webhook.salsa.debian.org/tagpending/{project_name}".format(
            project_name=project_name,
        )
    )

    webhooks_to_configure.append(
        "http://kgb.debian.net:9418/webhook/?channel=%23debian-golang",
    )

    for webhook_url in webhooks_to_configure:
        if webhook_url not in existing_webhooks_urls:
            print("    creating webhook...")
            create_webhook_on_salsa(project_id, webhook_url)
        else:
            print("    skipping webhook", webhook_url)


def configure_project(project):
    project_name = project["name"]
    print(
        "Configuring {project_name}...".format(
            project_name=project_name,
        )
    )

    configure_protected_branches(project)
    configure_webhooks(project)


def create_webhook_on_salsa(project_id, url):
    path = "/projects/{project_id}/hooks".format(
        project_id=project_id,
    )

    payload = {
        "url": url,
        "push_events": True,
    }

    request_to_salsa_api("POST", path, data=payload)


def protect_branch_on_salsa(project_id, branch, update=False):
    path = "/projects/{project_id}/protected_branches".format(
        project_id=project_id,
    )

    payload = {
        "name": branch,
        "push_access_level": 30,
        "merge_access_level": 30,
    }

    if update is True:
        delete_path = path + "/" + urllib.parse.quote(branch, safe='')
        request_to_salsa_api("DELETE", delete_path, data=None)

    request_to_salsa_api("POST", path, data=payload)


def configure_projects(projects):
    for project in projects:
        project_name = project["name"]

        try:
            configure_project(project)

        except Exception as ex:
            print("Could not configure project", project_name, ":", ex)
            log_failed_project_configuration(project_name, str(ex))


def log_failed_project_configuration(project_name, message):
    with open("failures.txt", "a") as f:
        f.write(
            "{project_name}: {message}\n".format(
                project_name=project_name,
                message=message,
            )
        )


def main():
    # Check for SALSA_TOKEN
    if "SALSA_TOKEN" not in os.environ:
        print("Please set the SALSA_TOKEN environement variable")
        sys.exit(1)

    try:
        projects = get_projects()
        configure_projects(projects)
    except KeyboardInterrupt:
            print("\nExiting...")


if __name__ == "__main__":
    main()

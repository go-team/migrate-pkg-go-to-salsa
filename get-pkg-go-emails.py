#!/usr/bin/python3

import requests
import re


def get_users():
    r = requests.get(
        "https://alioth.debian.org/project/memberlist.php?group_id=100873"
    )

    matches = re.findall(
        r'about="https://alioth.debian.org/users/([^\/]*)/',
        r.text,
    )
    return matches


def get_user_email(username):
    r = requests.get(
        "https://alioth.debian.org/users/{username}/".format(
            username=username,
        )
    )

    match = re.search(
        r"email_sha1.*>([^@]*)@nospam@([^<]*)<",
        r.text,
    )

    email = match.group(1).strip() + "@" + match.group(2).strip()

    return email


def main():
    users = get_users()

    for user in users:
        print(get_user_email(user))


if __name__ == "__main__":
    main()
